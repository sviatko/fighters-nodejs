import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    arenaWrapper: {
        display: "flex",
        justifyContent: "space-around",
        alignItems: "center",
        padding: theme.spacing(3)
    },
    indicatorWrapper: {
        width: "200px",
        height: "20px",
        textAlign: "center",
        color: "#fff",
        borderRadius: "4px",
        backgroundColor: "#ccc",
        border: "1px solid #000",
        boxShadow: "3px 3px #ccc"
    },
    indicator: {
        height: "100%",
        width: "100%",
        backgroundColor: "green",
        transition: "width 0.3s"
    },
    versusSign: {
        fontWeight: "bold"
    }
}));

export default function Arena({ fighterLeft, fighterRight }) {
    const classes = useStyles();
    
    document.addEventListener('keydown', function _listener(event) {
        map[event.code] = true;
  
        if(!event.repeat) {
          switch(true) {
            case  arrayContainsArray( Object.keys(map), controls.PlayerOneCriticalHitCombination ) &&
                  firstFighterTime === 0:
                  
                  fightService.criticalHit(firstFighter, secondFighter, secondFighterCopy, 'right', firstFighterTime);
                  firstFighterTime = 10;
            break;
            case  arrayContainsArray( Object.keys(map), controls.PlayerTwoCriticalHitCombination ) && 
                  secondFighterTime === 0:
                  
                  fightService.criticalHit(secondFighter, firstFighter, firstFighterCopy, 'left', secondFighterTime);
                  secondFighterTime = 10;
            break;
            case  map.hasOwnProperty(controls.PlayerOneAttack) &&
                  !map.hasOwnProperty(controls.PlayerOneBlock) && 
                  !map.hasOwnProperty(controls.PlayerTwoBlock) && 
                  map[controls.PlayerOneAttack]:
  
                  fightService.hit(firstFighter, secondFighter, secondFighterCopy, 'right');
            break;
            case  map.hasOwnProperty(controls.PlayerTwoAttack) &&
                  !map.hasOwnProperty(controls.PlayerOneBlock) && 
                  !map.hasOwnProperty(controls.PlayerTwoBlock) && 
                  map[controls.PlayerTwoAttack]:
  
                  fightService.hit(secondFighter, firstFighter, firstFighterCopy, 'left');
            break;
          }
  
          if(firstFighterCopy.health <= 0) {
            fightService.finishFight(_listener, interval, secondFighter, 'left', resolve);
          }
          if(secondFighterCopy.health <= 0) {
            fightService.finishFight(_listener, interval, firstFighter, 'right', resolve);
          }
        }
    }, true);
  
      document.addEventListener('keyup', event => {
        delete map[event.code];
     });

    //  const healthLeft = 

    return (
        <div className={classes.arenaWrapper}>
            <div>
                <div className={classes.indicatorWrapper}>
                    <div className={classes.indicator}></div>
                </div>
                <strong>{fighterLeft.name}</strong>
            </div>
            <div className={classes.versusSign}>vs</div>
            <div>
                <div className={classes.indicatorWrapper}>
                    <div className={classes.indicator}></div>
                </div>
                <strong>{fighterRight.name}</strong>
            </div>
        </div>
    );
}
