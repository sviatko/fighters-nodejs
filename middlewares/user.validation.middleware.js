const { user } = require("../models/user");
const UserService = require("../services/userService");
const validator = require("../services/validationService");
const { responseWithError } = require("../services/baseService");

const checkUsersExist = (req, res, next) => {
  const users = UserService.getAll();
  if (users) {
    res.body = [...users];
    next();
  } else {
    res.status(404).send({
      message: "Users not found",
      error: true,
    });
  }
};

const checkUserExists = (req, res, next) => {
  const usr = UserService.search({ id: req.params.id });

  if (usr) {
    res.body = usr;
    next();
  } else {
    responseWithError(res, "User not found");
  }
};

const createUserValid = (req, res, next) => {
  const data = { ...req.body };
  const messages = [];
  const fieldsToValidate = Object.keys(user).filter(
    (attribute) => attribute !== "id"
  );

  fieldsToValidate.map((field) => {
    if ((message = validator.validate(field, data, req))) {
      messages.push(message);
    }
  });

  if (
    messages.length === 0 &&
    fieldsToValidate.length === Object.keys(data).length
  )
    next();
  else
    responseWithError(res, "User entity to create is not valid", 400, messages);
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  const user = UserService.search({ id: req.params.id });

  if (user) {
    createUserValid(req, res, next);
  } else {
    responseWithError(res, "User not found", 404);
  }
};

exports.checkUserExists = checkUserExists;
exports.checkUsersExist = checkUsersExist;
exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
