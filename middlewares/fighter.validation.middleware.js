const { fighter } = require("../models/fighter");
const FighterService = require("../services/fighterService");
const { responseWithError } = require("../services/baseService");
const validator = require("../services/validationService");

const isFighterExists = (req, res, next) => {
  const fighter = FighterService.find(req.params.id);

  if (fighter) {
    res.body = fighter;
    next();
  } else {
    responseWithError(res, "Fighter not found");
  }
};

const isFightersExists = (req, res, next) => {
  const fighters = FighterService.getAll();

  if (fighters) {
    res.body = fighters;
    next();
  } else {
    responseWithError(res, "Fighters not found");
  }
};

const createFighterValid = (req, res, next) => {
  const data = { ...req.body };
  const messages = [];
  const fighterAttr = Object.keys(fighter).filter((attr) => attr !== "id");

  fighterAttr.map((attribute) => {
    if ((message = validator.validate(attribute, data, req))) {
      messages.push(message);
    }
  });

  if (messages.length === 0 && fighterAttr.length === Object.keys(data).length)
    next();
  else
    responseWithError(
      res,
      "Fighter entity to create is not valid",
      400,
      messages
    );
};

const updateFighterValid = (req, res, next) => {
  const fighter = FighterService.find(req.params.id);

  if (fighter) {
    createFighterValid(req, res, next);
  } else {
    responseWithError(res, "Fighter not found", 404);
  }
};

exports.isFighterExists = isFighterExists;
exports.isFightersExists = isFightersExists;
exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
