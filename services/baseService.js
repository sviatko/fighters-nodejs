const responseWithError = (res, message, status = 404, messages = null) => {
  const errorBody = {
    error: true,
    message: message,
  };

  if (messages) {
    errorBody.messages = { ...messages };
  }

  res.status(status).send(errorBody);
};

const prepareObjectToResponce = obj => {
  const updObj = {...obj};
  delete updObj.id;
  return updObj;
};

exports.prepareObjectToResponce = prepareObjectToResponce;
exports.responseWithError = responseWithError;
