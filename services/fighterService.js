const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAll() {
        const fighters = FighterRepository.getAll();

        if(fighters.length > 0) {
            return fighters;
        }

        return null;
    }

    store(data) {
        const fighter = FighterRepository.create(data);

        if(fighter) {
            return fighter;
        } else {
            throw Error('Fighter data is not valid');
        }
    }

    update(id, data) {
        const fighter = FighterRepository.update(id, data);

        if(fighter) {
            return fighter;
        } else {
            return null;
        }
    }

    find(id) {
        const fighter = FighterRepository.getOne({ id });

        if(fighter) {
            return fighter;
        } else {
            return null;
        }
    }

    search(search) {
        const fighter = FighterRepository.getOne(search);

        if(fighter) {
            return fighter;
        } else {
            return null;
        }
    }

    delete(id) {
        return FighterRepository.delete(id);
    }
}

module.exports = new FighterService();