const { Router } = require("express");
const UserService = require("../services/userService");
const { prepareObjectToResponce } = require("../services/baseService");
const { UserRepository } = require("../repositories/userRepository");
const {
  createUserValid,
  updateUserValid,
  checkUserExists,
  checkUsersExist,
} = require("../middlewares/user.validation.middleware");
const { responseMiddleware } = require("../middlewares/response.middleware");

const router = Router();

// TODO: Implement route controllers for user
router.get("/", checkUsersExist, (request, response, next) => {
  next();
}, responseMiddleware);

router.get(
  "/:id",
  checkUserExists,
  (request, response, next) => {
    next();
  }, responseMiddleware
);

router.post(
  "/",
  createUserValid,
  (request, response, next) => {
    const userData = { ...request.body };
    response.body = UserService.store(userData);
    next();
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateUserValid,
  (request, response, next) => {
    const userId = request.params.id;
    const userData = request.body;

    response.body = UserRepository.update(userId, userData);
    next();
  },
  responseMiddleware
);

router.delete(
  "/:id",
  checkUserExists,
  (request, response, next) => {
    UserRepository.delete(request.params.id);

    response.body = {
      error: false,
      message: "User deleted successfuly",
    };

    next();
  },
  responseMiddleware
);

module.exports = router;
