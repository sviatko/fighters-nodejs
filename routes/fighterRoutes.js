const { Router } = require("express");
const FighterService = require("../services/fighterService");
const { responseMiddleware } = require("../middlewares/response.middleware");
const {
  createFighterValid,
  updateFighterValid,
  isFightersExists,
  isFighterExists,
} = require("../middlewares/fighter.validation.middleware");
const { prepareObjectToResponce } = require("../services/baseService");

const router = Router();

router.get(
  "/",
  isFightersExists,
  (request, response, next) => {
    next();
  },
  responseMiddleware
);

router.get(
  "/:id",
  isFighterExists,
  (request, response, next) => {
    next();
  },
  responseMiddleware
);

router.post(
  "/",
  createFighterValid,
  (request, response, next) => {
    response.body = FighterService.store(request.body);
    next();
  },
  responseMiddleware
);

router.put(
  "/:id",
  updateFighterValid,
  (request, response, next) => {
    const data = { ...request.body };

    response.body = FighterService.update(request.params.id, data);
    next();
  },
  responseMiddleware
);

router.delete(
  "/:id",
  isFighterExists,
  (request, response, next) => {
    FighterService.delete(request.params.id);

    response.body = {
      error: false,
      message: "Fighter deleted successfuly",
    };

    next();
  },
  responseMiddleware
);

module.exports = router;
